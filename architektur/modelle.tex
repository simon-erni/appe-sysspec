\section{Modelle und Sichten}

Das Komponentendiagramm in Abbildung \ref{fig:komponentendiagramm} zeigt einen Überblick über die gesamte Architektur. Zwischen den beiden Komponenten \texttt{fbsservice} und \texttt{fbsdata} wurde eine Schnittstelle pro Fachobjekt erstellt. Zusätzlich wird vom \texttt{fbsdata} eine Schnittstelle \texttt{TransactionManager} für Transaktionen angeboten. Zwischen den Komponenten \texttt{fbsguiclient} und \texttt{fbsservice} wird ebenfalls eine Schnittstelle pro Fachobjekt definiert. Die Schnittstelle \texttt{AuthenticationEndpoint} dient zur Authentifizierung des Clients am Service. Über die Schnittstelle \texttt{Stock} wird das zentrale Lagersystem angebunden.

\begin{figure}[h!]
\centering
\includegraphics[width=\linewidth]{fig/komponentendiagramm}
\caption{Komponentendiagramm}
\label{fig:komponentendiagramm}
\end{figure}

\subsection{UI-Layer}

Der UI-Layer wird in Java-FX implementiert. Beim Bau solcher Java-FX Applikationen werden zwei 3rd Parties verwendet, welches die Applikations-Logik massiv vereinfachen soll. Diese werden nachfolgend kurz erläutert:

\begin{itemize}
	\item Google Guice: Dies ist ein Depedency Injection Framework. Die Controller können ihre Abhängigkeiten über die Injektion erhalten. Der Objekt-Baum muss nicht mehr mühsam von Hand erzeugt werden.
	\item Eventbus: Der Eventbus ist Teil von Google Guava. Dieser vereinfacht die Implementation des Observer-Patterns. Jeder kann auf den Eventbus Events senden und sich mit lediglich der Annotation \texttt{@Subscribe} für Events registrieren.
\end{itemize}

\begin{figure}[h!]
\centering
\includegraphics[width=0.7\linewidth]{fig/ui-interface/ui-layer-paketdiagramm}
\caption{Paketdiagram UI-Layer}
\label{fig:ui_layer_paketdiagramm}
\end{figure}

Auf der Abbildung \ref{fig:ui_layer_paketdiagramm} ist zu erkennen wie der UI-Layer aufgebaut wird. Nachfolgend werden die einzelnen Pakete erklärt:

\begin{description}
	\item[Controllers:] Die Controllers beinhalten die FXML-Dateien und die konkreten Controller Klassen. Controller sind für die Benutzer-Aktionen, Service-Calls, sowie die Beziehung zwischen Modell und UI zuständig.
	\item[Modell:] Dies sind die Modelle fürs GUI. Diese sind fürs GUI zugeschnitten. Die Modelle beinhalten die Properties, welche über die Controller in die Views gebunden werden.
	\item[Services:] Diese sind zuständig zuständig für die Daten. Sowohl Daten-Abfrage und auch Manipulation. Stellen die Schnittstelle zum Business-Layer dar.
	\item[Events:] Das sind POJOs, welche von den Controller verwendet werden. Diese werden für die Kommunikation der Controller untereinander über den Eventbus benötigt.
\end{description}

Jeder Controller kann der Ursprung für eine Benutzeraktion sein, welcher Einfluss auf irgendwelche Views hat. Diesen Views liegt auch je wieder ein Controller zugrunde. Damit nicht jeder Controller jeden anderen Controller kennen muss, verwenden wir hier den EventBus von Google. Jeder Controller wird automatisch registriert und kann über den Eventbus Events senden und sich für jedes Event auch wieder registrieren (Abbildung \ref{fig:ui_layer_controllers}).

\begin{figure}[h!]
\centering
\includegraphics[width=\linewidth]{fig/ui-interface/ui-layer-controllers}
\caption{Controller-Beziehung}
\label{fig:ui_layer_controllers}
\end{figure}

\newpage

\subsection{Business-Layer}

Die Hauptaufgabe des Business Layers ist die verschiedenen Komponenten wie Datenbank, GUI und Zentrallager flexibel untereinander zu verbinden. Dies bedeutet, dass eine grosse Anzahl von Abstraktionen der jeweiligen Komponenten im Einsatz sind, um dieses Ziel zu erreichen.

Abbildung \ref{fig:service-architektur} zeigt die Architektur des Service Layers. Die Gliederung in mehrere Layers innerhalb des Service Layers ist gut erkennbar. Zuoberst sind die Schnittstellen zum UI-Layer angesiedelt, wobei zwischen den Services und den Schnittstellen noch ein sog. \emph{Translator} zu finden ist. Dessen Aufgabe ist es, die eingehenden Anfragen in das interne Datenformat zu verwandeln und die ausgehenden Objekte in das Schnittstellenformat umzuwandeln. So ist die Umwandlung der Objekte zentral vorhanden und intern kann nun nur noch mit den eigenen Datentypen gearbeitet werden.

\begin{figure}[h!]
\centering
\includegraphics[width=\linewidth]{fig/service-architektur}
\caption{Ausschnitt Klassendiagramm Service Layer}
\label{fig:service-architektur}
\end{figure}

Damit kein manipulierter Client unautorisiert auf Daten zugreifen kann, wird die Authentifizierung und Autorisierung im Business-Layer verwaltet. Dadurch hat der Server immer die vollständige Kontrolle über die Anfragen der Clients. Im UI werden je nach Benutzer-Rolle gewisse Elemente ausgeblendet. Dadurch muss die Autorisierungslogik teilweise doppelt implementiert werden. Diese Duplikation wurde bewusst in Kauf genommen um den Bedienkomfort zu steigern.

\newpage

\subsection{Data-Layer}
\label{sec:architektur-datenlayer}

Der Daten-Layer orientiert sich am Repository-Pattern\footnote{\href{http://martinfowler.com/eaaCatalog/repository.html}{http://martinfowler.com/eaaCatalog/repository.html}} von Martin Fowler. Bei diesem Pattern verhält sich der Daten-Layer wie eine Liste von Fachobjekten. Der Benutzer verwendet das Daten-Layer wie eine normale Liste und muss sich nicht um die darunterliegende Persistenz kümmern. Für den Benutzer des Daten-Layers ist es egal, ob zum Speichern seiner Objekte JPA oder z.B. ein Webservice genutzt wird. Dadurch bleibt der Daten-Layer erweiterbar ohne die Schnittstelle verändern zu müssen. Listing \ref{lst:crud-interface} zeigt die Schnittstelle zum Daten-Layer, welche allen Fachobjekten zur Verfügung steht.
\begin{lstlisting}[caption={CRUD-Interface},label=lst:crud-interface]
/**
* @param <E> Der Typ der Entität
* @param <K> Der Typ des Primärschlüssels der Entität
*/
public interface CrudRepository<E, K> {

	E create(E entity);

	E update(E entity);

	E findOne(K primaryKey);

	void delete(E entity);

}
\end{lstlisting}
Ein Repository stellt die CRUD\footnote{Create Read Update Delete}-Operationen für das entsprechende Fachobjekt zur Verfügung. Dieses Repository leitet vom \texttt{CrudRepository} aus Listing \ref{lst:crud-interface} ab, damit ihm die CRUD-Methoden zur Verfügung stehen. Über Generics wird dem \texttt{CrudRepository} mitgeteilt welches Fachobjekt verwendet wird. Im Repository können noch spezifische Methoden für das Fachobjekt definiert werden. Listing \ref{lst:item-interface} zeigt ein Beispiel für ein Produkt-Repository.
\begin{lstlisting}[caption={Produkt-Repository},label=lst:item-interface]
public interface ItemRepository extends CrudRepository<Item, Integer> {
	void count();
}
\end{lstlisting}
Möchte man nun auf eine Datenbank zugreifen muss das \texttt{CrudRepository} implementiert werden. Für JPA könnte die \texttt{create()}-Methode wie in Listing \ref{lst:jpa-repository} aussehen.
\begin{lstlisting}[caption={JPA-Repository},label=lst:jpa-repository]
public abstract class AbstractJpaRepository<E, K> implements CrudRepository<E, K> {
	@Override
	public E create(final E entity) {
		entityManager.persist(entity);
		return entity;
	}
}
\end{lstlisting}
Die Implementation vom \texttt{ItemRepository} leitet vom \texttt{JpaRepository} ab und hat somit alle CRUD-Methoden zur Verfügung. Zusätzlich werden hier noch die spezifischen Methoden vom \texttt{ItemRepository} implementiert. Listing \ref{lst:jpa-item-repository} zeigt wie die Implementation in JPA aussehen könnte.
\begin{lstlisting}[caption={JPA-Produkt-Repository},label=lst:jpa-item-repository]
public class JpaItemRepository extends AbstractJpaRepository<Item, Integer> implements ItemRepository {
	@Override
	public Long count() {
		// Count Implementation
	}
}
\end{lstlisting}
Durch dieses Pattern lässt sich Code-Duplikation vermeiden weil die CRUD-Methoden, welche jedes Fachobjekt benötigt, im \texttt{JpaRepository} implementiert wurden. Die gezeigten Beispiele sollen nur der Erklärung dienen und wurden im Code nicht genau so umgesetzt.

Damit weitere Persistenz-Technologien hinzugefügt werden können, wird die Erzeugung eines Repositorys über eine Abstrakte Fabrik\footnote{\href{http://de.wikipedia.org/wiki/Abstrakte_Fabrik}{http://de.wikipedia.org/wiki/Abstrakte\_Fabrik}} erledigt. Die abstrakte Fabrik beinhaltet abstrakte Methoden für jedes Repository. Listing \ref{lst:abstrakte-fabrik} zeigt ein Ausschnitt dieser abstrakten Fabrik. 
\begin{lstlisting}[caption={Abstrakte Fabrik für Repositories},label=lst:abstrakte-fabrik]
public abstract class AbstractRepositoryFactory {
	public abstract ItemRepository getItemRepository();

	public static AbstractRepositoryFactory getRepositoryFactory() {
		AbstractRepositoryFactory factory = null;
		switch (whichFactory) {
			case JPA:
				return new JpaRepositoryFactory();
		}
	}
}
\end{lstlisting}
Eine Persistenz-Technologie muss die Fabrik zur Verfügung stellen welche diese abstrakten Methoden implementiert und ein entsprechendes Repository zurückliefert. Über die statische Methoden \texttt{getRepositoryFactory} kann die Implementierung ausgewählt werden. Die Methode gibt zur Zeit nur eine Fabrik für JPA zurück, kann jedoch einfach um weitere Technologien (z.B. MongoDB) erweitert werden. Der Wechsel zwischen den Technologien kann z.B. über eine Properties-Datei eingestellt werden.

Die Benutzung dieser Schnittstelle ist im Abschnitt \ref{sec:schnittstelle-datenlayer} ausführlich beschrieben.